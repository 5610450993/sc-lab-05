import model.Book;
import model.Library;
import model.ReferenceBook;


public class User {
	public static void main(String[] args) { 
		Library libB = new Library(); 
		Book book1 = new Book("JAVA","Leonardo","1999");
		Book book2 = new Book("MATH","Davinci","2000");
		libB.addBook(book1);libB.addBook(book2);
		System.out.println("Book balance: "+libB.numBook()); 
		libB.borrowBook("JAVA");
		System.out.println("Borrow JAVA ");
		System.out.println("Book balance: "+libB.numBook());
		libB.borrowBook("JAVA");
		System.out.println(libB.showBook());
		libB.reBook("JAVA");
		System.out.println("Return JAVA ");
		System.out.println("Book balance: "+libB.numBook());
		System.out.println(libB.showBook());
		
		int vip = 3;
		ReferenceBook refBook1 = new ReferenceBook("108 win girls","Mark","2012",vip);
		ReferenceBook refBook2 = new ReferenceBook("Magazine Gays","Zuckerberg","2499",vip);
		libB.addRefBook(refBook1);libB.addRefBook(refBook2);
		libB.borrowRefBook("108 win girls","Student");
		System.out.println("ReferenceBook balance: "+libB.numRefBook());
		libB.borrowRefBook("Magazine Gays","Teacher");
		System.out.println("Borrow Magazine Gays");
		System.out.println("ReferenceBook balance: "+libB.numRefBook()); 
		libB.borrowRefBook("108 win girls","Teacher");
		System.out.println("Borrow 108 win girls");
		System.out.println("ReferenceBook balance: "+libB.numRefBook()); 
		System.out.println(libB.showRefBook());
		libB.reRefBook("Magazine Gays");
		System.out.println("ReferenceBook balance: "+libB.numRefBook()); 
		System.out.println(libB.showRefBook());
		} 

}