package model;

public class ReferenceBook{
	private String title;
	private String author;
	private String year;
	private boolean status;
	private int vip;
	
	public ReferenceBook(String title,String author,String year,int vip){
		this.title = title;
		this.author = author;
		this.year = year;
		this.vip = vip;
		status = true;
	}
	public String getTitle(){
		return title;
	}
	
	public String getAuthor(){
		return author;
	}
	
	public String getYear(){
		return year;
	}
	
	public  boolean getStatus(){
		return status;
	}
	
	public  void setStatus(boolean status){
		this.status = status;
	}
	public String toString(){
		return title+","+author+","+year+","+status;
	}
	
	public int getVIP(){
		return vip;
	}
}
