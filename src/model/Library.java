package model;

import java.util.ArrayList;


public class Library {
	ArrayList<Book> book;
	ArrayList<ReferenceBook> refBook;
	
	public Library(){
		this.book = new ArrayList<Book>();
		this.refBook = new ArrayList<ReferenceBook>();
	}
	
	// Book
	public void addBook(Book book){
		this.book.add(book);
	}
		
	public int numBook(){
		int count = 0;
		for(int i=0 ; i < book.size() ; i++){
			if(book.get(i).getStatus()==true) count++;
		}
		return count;
	}	
	
	public void borrowBook(String title){
		boolean found = false;
		for(int i=0 ; i < book.size() && !found; i++){
			if(title.equals(book.get(i).getTitle())){
				if(book.get(i).getStatus()==false){
					System.out.println("Book: Have borrow");
				}
				else{
					book.get(i).setStatus(false);
				}
				found = true;
			}
		}
	}
	
	public void reBook(String title){
		for(int i=0 ; i < book.size() ; i++){
			if(title.equals(book.get(i).getTitle())){
				book.get(i).setStatus(true);
			}
		}
	}
	
	public String showBook(){
		String show = "Show Book"+"\n";
		for(int i=0 ; i < book.size() ; i++){
			show += "BOOK["+(i+1)+"]: "+book.get(i)+"\n";
		}
		return show;
	}
	
	// ReferenceBook
	public void addRefBook(ReferenceBook refBook){
		this.refBook.add(refBook);
	}
	
	public int numRefBook(){
		int count = 0;
		for(int i=0 ; i < refBook.size() ; i++){
			if(refBook.get(i).getStatus()==true) count++;
		}
		return count;
	}	
	
	public int checkVIP(String member){
		if(member.equalsIgnoreCase("teacher")){
			return 3;
		}
		else if(member.equalsIgnoreCase("officer")){
			return 2;
		}
		else{
			return 1;
		}
	}
	
	public void borrowRefBook(String title,String member){
		boolean found = false;
		int vip = checkVIP(member);
		for(int i=0 ; i < refBook.size() && !found; i++){
			if(title.equals(refBook.get(i).getTitle())&& refBook.get(i).getVIP()<= vip){
				if(refBook.get(i).getStatus()==false){
					System.out.println("RefBook: Have borrow");
				}
				else{
					refBook.get(i).setStatus(false);
				}
				found = true;
			}
		}
	}
	
	public void reRefBook(String title){
		for(int i=0 ; i < refBook.size() ; i++){
			if(title.equals(refBook.get(i).getTitle())){
				refBook.get(i).setStatus(true);
			}
		}
	}
	
	public String showRefBook(){
		String show = "Show ReferenceBook"+"\n";
		for(int i=0 ; i < refBook.size() ; i++){
			show += "REFERENCE_BOOK["+(i+1)+"]: "+refBook.get(i)+"\n";
		}
		return show;
	}
	
	
	public String toString(){
		return book.toString();
	}
}
