package model;

public class Book {
	private String title;
	private String author;
	private String year;
	private boolean status;
	
	public Book(){}
	public Book(String title,String author,String year){
		this.title = title;
		this.author = author;
		this.year = year;
		status = true;
	}
	
	public String getTitle(){
		return title;
	}
	
	public String getAuthor(){
		return author;
	}
	
	public String getYear(){
		return year;
	}
	
	public  boolean getStatus(){
		return status;
	}
	
	public  void setStatus(boolean status){
		this.status = status;
	}
	public String toString(){
		return title+","+author+","+year+","+status;
	}
}
